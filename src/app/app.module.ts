import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Component01Component } from './component01/component01.component';
import { Component02Component } from './component02/component02.component';
import { NothingFoundComponent } from './nothing-found/nothing-found.component';

@NgModule({
  declarations: [
    AppComponent,
    Component01Component,
    Component02Component,
    NothingFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
