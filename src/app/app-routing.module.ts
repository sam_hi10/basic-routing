import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Component01Component } from './component01/component01.component';
import { Component02Component } from './component02/component02.component';
import { NothingFoundComponent } from './nothing-found/nothing-found.component';

const routes: Routes = [
  {path: '', redirectTo: '1', pathMatch: 'full'},
  {path: '1', component: Component01Component},
  {path: '2', component: Component02Component},
  {path: '**', component: NothingFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
